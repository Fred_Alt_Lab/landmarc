#!/usr/bin/env python3
import os, re, sys, logging, csv, multiprocessing, time

import pandas as pd
from glob import glob
from itertools import groupby
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from collections import OrderedDict, Counter

from HTGTSrep.lib import loggingRun, getInputSeq, getCDR, getGermdict, \
                         collapse_db, profile_DNAmut

records = pd.read_csv('test.db.xls', sep="\t", low_memory=False)
records = collapse_db(records, 'partial', 'F')
records.to_csv('test.collapse.xls', sep="\t", index=False)
